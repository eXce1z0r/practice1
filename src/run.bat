@ echo off

mkdir "../bin/"

javac -d "../bin" ua/nure/sidak/practice1/Part1.java

javac -d "../bin" ua/nure/sidak/practice1/Part2.java

javac -d "../bin" ua/nure/sidak/practice1/Part3.java

javac -d "../bin" ua/nure/sidak/practice1/Part4.java

javac -d "../bin" ua/nure/sidak/practice1/Part5.java

javac -d "../bin" -classpath "../bin/" ua/nure/sidak/practice1/Demo.java

java -classpath "../bin" ua.nure.sidak.practice1.Demo 3 4 14 21 2 ab c def gijklmnop

rmdir ..\bin\ /s /q
