package ua.nure.sidak.practice1;

public class Part4
{
	public static void main(String[] args)
	{
		int val1 = 0;
			
		int val2 = 0;
		
		if(args.length == 2)
		{
			val1 = Integer.parseInt(args[0]);
				
			val2 = Integer.parseInt(args[1]);
				
			int smallerVal = val1 > val2?val2:val1;
				
			int gCF = 1; // greatest common factor
				
			for(int i = smallerVal; i > 1; i--)	//	calculation of greatest common factor
			{
				if(val1 % i == 0 && val2 % i == 0)
				{
					gCF = i;
					break;
				}
			}
				
			int lCM = (val1 * val2) / gCF;// lowest common multiple
				
			System.out.println("Lowest common multiple: " + lCM);
		}		
	}
}