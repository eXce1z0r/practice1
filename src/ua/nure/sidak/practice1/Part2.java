package ua.nure.sidak.practice1;

public class Part2
{
	public static void main(String[] args) throws NumberFormatException
	{
		if(args.length == 2)
		{
			Double firstVal = 0.;
			Double secondVal = 0.;
			
			firstVal = Double.parseDouble(args[0]);
			
			secondVal = Double.parseDouble(args[1]);
			
			System.out.println(firstVal + " - " + secondVal + " = " + (firstVal - secondVal));
		}
	}
}