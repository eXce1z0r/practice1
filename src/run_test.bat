@ echo off

rmdir ..\bin\ /s /q

mkdir "../bin/"

javac -d "../bin" ua/nure/sidak/practice1/Part1/HelloWorldClass.java

javac -d "../bin" ua/nure/sidak/practice1/Part2/SubtractionClass.java

javac -d "../bin" ua/nure/sidak/practice1/Part3/ConsoleInputPrinter.java

javac -d "../bin" ua/nure/sidak/practice1/Part4/LCMClass.java

javac -d "../bin" ua/nure/sidak/practice1/Part5/BinaryValueOneCounter.java

java -classpath "../bin" ua.nure.sidak.practice1.Part1.HelloWorldClass

java -classpath "../bin" ua.nure.sidak.practice1.Part2.SubtractionClass 3 2

java -classpath "../bin" ua.nure.sidak.practice1.Part3.ConsoleInputPrinter ab c def gijklmnop

java -classpath "../bin" ua.nure.sidak.practice1.Part4.LCMClass 14 21

java -classpath "../bin" ua.nure.sidak.practice1.Part5.BinaryValueOneCounter 2

rmdir ..\bin\ /s /q
