package ua.nure.sidak.practice1;

import ua.nure.sidak.practice1.Part1;
import ua.nure.sidak.practice1.Part2;
import ua.nure.sidak.practice1.Part3;
import ua.nure.sidak.practice1.Part4;
import ua.nure.sidak.practice1.Part5;

public class Demo
{
	public static void main(String[] args)
	{
		 //HelloWorldClass
		 Part1.main(new String[0]);
		 //SubtractionClass
		 Part2.main(new String[] {args[0], args[1]});
		 //ConsoleInputPrinter
		 Part3.main(args);
		 //LCMClass
		 Part4.main(new String[] {args[2], args[3]});
		 //BinaryValueOneCounter
		 Part5.main(new String[] {args[4], args[5]});
		 
	}
}