package ua.nure.sidak.practice1;

import java.lang.Exception;
import java.lang.RuntimeException;

public class Part5
{
	public static void main(String[] args)
	{
		int counter = 0;
		if(args.length > 0)
		{
			int decVal = 0;
			
			decVal = Integer.parseInt(args[0]);
			
			while(decVal > 0)	//	repeat while "remainderFromDivision" will be completely divided by 2
			{
				int remainderFromDivision = decVal % 2;
				if(remainderFromDivision == 1)
				{
					counter++;
				}		

				decVal /= 2;
			}
				
			System.out.println("Binary one in this value: " + counter);
		}
	}
}